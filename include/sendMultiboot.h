#ifndef APOTRIS_SENDMULTIBOOT_H
#define APOTRIS_SENDMULTIBOOT_H

#include "LinkCableMultiboot.hpp"
#include "platform.hpp"

LinkCableMultiboot::Result sendMultiboot();

#endif // APOTRIS_SENDMULTIBOOT_H
