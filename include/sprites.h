#include "sprite10tiles_bin.h"
#include "sprite11tiles_bin.h"
#include "sprite12tiles_bin.h"
#include "sprite13tiles_bin.h"
#include "sprite14tiles_bin.h"
#include "sprite15tiles_bin.h"
#include "sprite16tiles_bin.h"
#include "sprite17tiles_bin.h"
#include "sprite18tiles_bin.h"
#include "sprite19tiles_bin.h"
#include "sprite1tiles_bin.h"
#include "sprite20tiles_bin.h"
#include "sprite21tiles_bin.h"
#include "sprite22tiles_bin.h"
#include "sprite23tiles_bin.h"
#include "sprite24tiles_bin.h"
#include "sprite25tiles_bin.h"
#include "sprite26tiles_bin.h"
#include "sprite27tiles_bin.h"
#include "sprite28tiles_bin.h"
#include "sprite29tiles_bin.h"
#include "sprite2tiles_bin.h"
#include "sprite36tiles_bin.h"
#include "sprite37tiles_bin.h"
#include "sprite38tiles_bin.h"
#include "sprite39tiles_bin.h"
#include "sprite3tiles_bin.h"
#include "sprite40tiles_bin.h"
#include "sprite41tiles_bin.h"
#include "sprite42tiles_bin.h"
#include "sprite43tiles_bin.h"
#include "sprite44tiles_bin.h"
#include "sprite45tiles_bin.h"
#include "sprite46tiles_bin.h"
#include "sprite47tiles_bin.h"
#include "sprite48tiles_bin.h"
#include "sprite49tiles_bin.h"
#include "sprite4tiles_bin.h"
#include "sprite50tiles_bin.h"
#include "sprite51tiles_bin.h"
#include "sprite52tiles_bin.h"
#include "sprite53tiles_bin.h"
#include "sprite54tiles_bin.h"
#include "sprite55tiles_bin.h"
#include "sprite56tiles_bin.h"
#include "sprite57tiles_bin.h"
#include "sprite58tiles_bin.h"
#include "sprite59tiles_bin.h"
#include "sprite5tiles_bin.h"
#include "sprite60tiles_bin.h"
#include "sprite6tiles_bin.h"
#include "sprite7tiles_bin.h"
#include "sprite8tiles_bin.h"
#include "sprite9tiles_bin.h"
#include "sprite_pal_bin.h"

#include "title1tiles_bin.h"
#include "title2tiles_bin.h"
#include "title_pal_bin.h"

#define paletteLen 512
extern const uint16_t palette[2][256];

extern const uint8_t* mini[9];
extern const uint8_t* boneMini[];

extern const uint8_t* moveSpriteTiles[8];

extern const uint8_t* placeEffectTiles[15];
extern const uint8_t* classicTiles[2][8];
extern const uint8_t* meterTiles[4];

extern const uint16_t nesPalette[10][4];
extern const uint16_t monoPalette[2][4];
extern const uint16_t arsPalette[2][8][4];

extern const uint16_t fontPalettes[2][2][5];
