Customizing songs:
    Add or remove songs in the 'audio' folder. Songs need to be in a tracker
    format (specifically either .mod, .it, .xm or .s3m). The limits are
    that songs need to use 8 channels at most, the max ROM size for GBA (32MB)
    and the game has a software limit of 255 songs for the menu and game categories each.
    You can specify if the songs are going to play during menus or in-game by
    adding 'MENU_' or 'GAME_' respectively in front of the song's name.

Customizing sound effects:
    Replace any of the sound effects in the 'audio' folder with ones with the
    same file name.

WINDOWS:
    After any changes, run 'WINDOWS_build.bat' to add them to the ROM.

LINUX:
    Python is required to build songs. After any changes, run 'LINUX_or_MAC_build.sh' to add them to the ROM.

MAC:
    Run the LINUX_or_MAC_build.sh to add any changes to the ROM.
